package com.example.demo;

public class BlockedAccount extends Exception{

    public BlockedAccount() {}

    public BlockedAccount(String message)
    {
        super(message);
    }
}
