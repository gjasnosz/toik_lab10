package com.example.demo.service;

import com.example.demo.BlockedAccount;
import com.example.demo.dto.LoginDto;
import com.example.demo.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository = new UserRepository();

    public boolean checkLogin(String login, String password) throws BlockedAccount {
        return this.userRepository.checkLogin(login, password);
    }
}
